velliv-dwh-cloudfunctions-api

Dev Setup (powershell)
virtualenv venv
.\venv\Scripts\activate.ps
pip install -r requirements.txt

To run locally in powershell:
$env:FLASK_APP = "main.py"
python -m flask run

Invoke-WebRequest -Uri https://europe-west1-velliv-dwh-development.cloudfunctions.net/velliv-dwh-api-bigquery-test -Method POST -ContentType "application/json" -Body '{"cvr":"31613280"}' -Headers @{Authorization = "Bearer $(gcloud auth print-identity-token)"}

Manual deploy
gcloud functions deploy velliv-dwh-api-bigquery-test --entry-point=bigquery_test --runtime=python37 --trigger-http --region=europe-west1

Function created with limited-access IAM policy. To enable unauthorized access consider "gcloud alpha functions add-iam-policy-binding velliv-dwh-api --region=europe-west1 --member=allUsers --role=roles/cloudfunctions.invoker"
Single user:
gcloud alpha functions add-iam-policy-binding velliv-dwh-api-bigquery-test --region=europe-west1 --member=user:g86717@konsulent.velliv.dk --role=roles/cloudfunctions.invoker