import json

from flask import Flask, escape
from google.cloud import bigquery

app = Flask(__name__)
project_id = 'velliv-dwh-development'


@app.route('/', methods=['GET'])
def hello_http(request):
    """HTTP Cloud Function.
    Args:
        request (flask.Request): The request object.
        <https://flask.palletsprojects.com/en/1.1.x/api/#incoming-request-data>
    Returns:
        The response text, or any set of values that can be turned into a
        Response object using `make_response`
        <https://flask.palletsprojects.com/en/1.1.x/api/#flask.make_response>.
    """
    request_json = request.get_json(silent=True)
    request_args = request.args

    if request_json and 'name' in request_json:
        name = request_json['name']
    elif request_args and 'name' in request_args:
        name = request_args['name']
    else:
        name = 'World'
    return 'Hello {}!'.format(escape(name))


@app.route('/bigquery/test', methods=['POST'])
def bigquery_test(request):
    request_json = request.get_json(silent=True)
    bigquery_client = bigquery.Client(project=project_id)

    # Example cvr: 31613280
    query = f"SELECT FMNAV1 AS FirmaNavn FROM dm_kunde.kunde_naid_202006 WHERE CVR_NR = {request_json['cvr']}"

    query_job = bigquery_client.query(query)
    result = query_job.result()
    print(result)
    firma_navn = "Ukendt CVR"
    for row in result:
        firma_navn = row.FirmaNavn
    return f'Firma navn: {firma_navn}'


@app.route('/get_customer_policies_with_details', methods=['POST'])
def get_customer_policies_with_details(request):
    request_json = request.get_json(silent=True)
    request_args = request.args
    bigquery_client = bigquery.Client(project=project_id)
    print("request.json: " + str(request_json))
    print("request.args: " + str(request_args))

    if request_json and 'cpr' in request_json:
        cpr = request_json['cpr']
    elif request_args and 'cpr' in request_args:
        cpr = request_args['cpr']
    else:
        return "Cpr ikke fundet i request"

    # Example cvr: 31613280
    query = f"""SELECT kunde.cpr_nr, kunde.fornavn, kunde.efternavn, bestand.PoliceNr, bestand.Livscyklus,
                bestand.Total_Prm, bestand.Pensionsgivende_loen, bestand.Total_PrmPct, bestand.Obligatorisk_Pct,
                bestand.Frivillig_Pct, bestand.Prm_til_DinKapital, bestand.Prm_til_Link, bestand.Prm_til_Hoejrente,
                bestand.Vaekst_OpsLivrentePlus_SK1_SALDO, bestand.Vaekst_OpsLivrentePlus_SK7_SALDO,
                bestand.Vaekst_OpsRate_SK2_SALDO, bestand.Vaekst_OpsRate_SK7_SALDO, bestand.Vaekst_OpsSum_SK3_SALDO,
                bestand.Vaekst_OpsSum_SK7_SALDO, bestand.Vaekst_OpsSum_SK33_SALDO,
                bestand.Link_OpsLivrentePlus_SK1_SALDO,	bestand.Link_OpsLivrentePlus_SK7_SALDO,
                bestand.Link_OpsRate_SK2_SALDO,	bestand.Link_OpsRate_SK7_SALDO,	bestand.Link_OpsSum_SK3_SALDO,
                bestand.Link_OpsSum_SK33_SALDO,	bestand.Link_OpsSum_SK7_SALDO,	bestand.HR_OpsLivrentePlus_SK1_SALDO,
                bestand.HR_OpsLivrentePlus_SK7_SALDO,	bestand.HR_OpsRate_SK2_SALDO,	bestand.HR_OpsRate_SK7_SALDO,
                bestand.HR_OpsSum_SK3_SALDO,	bestand.HR_OpsSum_SK33_SALDO,	bestand.HR_OpsSum_SK7_SALDO,
                bestand.DinKap_OpsLivrentePlus_SK1_SALDO, bestand.DinKap_OpsLivrentePlus_SK7_SALDO,
                bestand.DinKap_OpsRate_SK2_SALDO, bestand.DinKap_OpsRate_SK7_SALDO, bestand.DinKap_OpsSum_SK3_SALDO,
                bestand.DinKap_OpsSum_SK33_SALDO, bestand.DinKap_OpsSum_SK7_SALDO, bestand.Rest_SALDO,
                bestand.Sum_SALDO, bestand.AktivMG_saldo, bestand.AktivUG_saldo, bestand.IndexMG_saldo,
                bestand.IndexUG_saldo, bestand.LinkUG_saldo, bestand.HR_saldo, bestand.DK_saldo
                FROM
                `velliv-dwh-development.dim_kunde.person` kunde
                inner join `velliv-dwh-development.dm_bestand.bestand_n16` bestand on kunde.cpr_nr = bestand.cpr_nr
                where kunde.cpr_nr='{cpr}' and bestand.DMM_EndTime='5999-12-31T23:59:59'"""

    query_job = bigquery_client.query(query)
    records = [dict(row) for row in query_job]
    if not records:
        return 'CPR ikke fundet.'
    else:
        json_obj = json.dumps(str(records))
        return json_obj
